# ~/.bashrc
# umask 022

# If shell /= Interactive Mode, return
[ -z "$PS1" ] && return

export LS_OPTIONS='--color=auto'
export PS1="\[\e[91m\]\u\[\e[m\]@\h:\[\e[36m\]\w\[\e[m\]\$ "

alias ls='ls $LS_OPTIONS'

cd() {
    builtin cd "$@" && ls;
}