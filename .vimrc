" Plugins	
"	call plug#begin('~/dotfiles/vim/plugged')
"	Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
"	Plug 'scrooloose/nerdcommenter'
"	Plug 'vim-syntastic/syntastic'
"	Plug 'vim-airline/vim-airline'
"	Plug 'vim-airline/vim-airline-themes'
"	Plug 'tpope/vim-fugitive'
"	Plug 'python-mode/python-mode', { 'branch': 'develop'}
"	call plug#end()
    
    autocmd BufNewFile,BufRead *.asm set filetype=nasm
	let g:syntastic_ignore_files = ['\.py$']
	let g:pymode_python = 'python3'

" REPLACE TABS WITH SPACES
"
" https://stackoverflow.com/questions/234564/tab-key-4-spaces-and-auto-indent-after-curly-braces-in-vim
	filetype plugin indent on
" show existing tab with 4 spaces width
"    set tabstop=4
" when indenting with '>', use 4 spaces width
"    set shiftwidth=4
" On pressing tab, insert 4 spaces
"    set expandtab

" MOVING LINES UP OR DOWN
"
" https://vim.fandom.com/wiki/Moving_lines_up_or_down
    nnoremap <C-S-j> :m .+1<CR>==
    nnoremap <C-S-k> :m .-2<CR>==
    inoremap <C-S-j> <Esc>:m .+1<CR>==gi
    inoremap <C-S-k> <Esc>:m .-2<CR>==gi
    vnoremap <C-S-j> :m '>+1<CR>gv=gv
    vnoremap <C-S-k> :m '<-2<CR>gv=gv

" Plugin options
"	let g:NERDSpaceDeLims = 1
"	 Airline
"		let g:airline_theme='murmur'

" General Vim settings
	syntax on
	let mapleader=","
	set autoindent
	set tabstop=4
	set shiftwidth=4
	set dir=/tmp/
	set relativenumber 
	set number
	set background=dark
	set path+=**

" ECLIPSE SUPPORT
"	set nocompatible
	filetype plugin on


	inoremap " ""<left>
"	inoremap ' ''<left>
	inoremap ( ()<left>
	inoremap [ []<left>
	inoremap { {}<left>
	inoremap {<CR> {<CR>}<ESC>O
	inoremap {;<CR> {<CR>};<ESC>O


	autocmd Filetype html setlocal sw=2 expandtab
	autocmd Filetype javascript setlocal sw=4 expandtab
	" autocmd VimEnter * NERDTree

	set cursorline
	hi Cursor ctermfg=White ctermbg=Yellow cterm=bold guifg=white guibg=yellow gui=bold
	hi StatusLine ctermbg=Lightgrey ctermfg=Black 
	hi StatusLineNC ctermbg=Lightgrey ctermfg=Black 
	hi VertSplit ctermbg=Lightgrey ctermfg=Black 

	set hlsearch
	nnoremap <C-l> :nohl<CR><C-l>:echo "Search Cleared"<CR>
	nnoremap <C-c> :set norelativenumber<CR>:set nonumber<CR>:echo "Line numbers turned off."<CR>
	nnoremap <C-n> :set relativenumber<CR>:set number<CR>:echo "Line numbers turned on."<CR>

	nnoremap n nzzzv
	nnoremap N Nzzzv

	nnoremap H 0
	nnoremap L $
	nnoremap J G
	nnoremap K gg

	map <tab> %
	map <C-b> :NERDTreeToggle<CR>

	set backspace=indent,eol,start

	nnoremap <Space> za
	nnoremap <leader>z zMzvzz

	nnoremap vv 0v$

	set listchars=tab:\|\ 
	nnoremap <leader><tab> :set list!<cr>
	set pastetoggle=<F2>
	set mouse=a
	set incsearch

" Language Specific
	" Tabs
	"	so ~/dotfiles/vim/tabs.vim

	" General
		inoremap <leader>for <esc>Ifor (int i = 0; i < <esc>A; i++) {<enter>}<esc>O<tab>
		inoremap <leader>if <esc>Iif (<esc>A) {<enter>}<esc>O<tab>
		

	" Java
		inoremap <leader>sys <esc>ISystem.out.println(<esc>A);
		vnoremap <leader>sys yOSystem.out.println(<esc>pA);

	" Java
		inoremap <leader>con <esc>Iconsole.log(<esc>A);
		vnoremap <leader>con yOconsole.log(<esc>pA);

	" C++
		inoremap <leader>cout <esc>Istd::cout << <esc>A << std::endl;
		vnoremap <leader>cout yOstd::cout << <esc>pA << std:endl;

	" C
		inoremap <leader>out <esc>Iprintf(<esc>A);<esc>2hi
		vnoremap <leader>out yOprintf(, <esc>pA);<esc>h%a

	" Typescript
		autocmd BufNewFile,BufRead *.ts set syntax=javascript
		autocmd BufNewFile,BufRead *.tsx set syntax=javascript

	" Markup
		inoremap <leader>< <esc>I<<esc>A><esc>yypa/<esc>O<tab>


" File and Window Management 
	inoremap <leader>w <Esc>:w<CR>
	nnoremap <leader>w :w<CR>

	inoremap <leader>q <ESC>:q<CR>
	nnoremap <leader>q :q<CR>

	inoremap <leader>x <ESC>:x<CR>
	nnoremap <leader>x :x<CR>

	nnoremap <leader>e :Ex<CR>
	nnoremap <leader>t :tabnew<CR>:Ex<CR>
	nnoremap <leader>v :vsplit<CR>:w<CR>:Ex<CR>
	nnoremap <leader>s :split<CR>:w<CR>:Ex<CR>

" Return to the same line you left off at
	augroup line_return
		au!
		au BufReadPost *
			\ if line("'\"") > 0 && line("'\"") <= line("$") |
			\	execute 'normal! g`"zvzz' |
			\ endif
	augroup END

" Auto load
	" Triger `autoread` when files changes on disk
	" https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#383044
	" https://vi.stackexchange.com/questions/13692/prevent-focusgained-autocmd-running-in-command-line-editing-mode
	autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * if mode() != 'c' | checktime | endif
	set autoread 
	" Notification after file change
	" https://vi.stackexchange.com/questions/13091/autocmd-event-for-autoread
	autocmd FileChangedShellPost *
	  \ echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None

" Future stuff
	"Swap line
	"Insert blank below and above

